const Path = require('path');
const vuePlugin = require('@vitejs/plugin-vue')

const { defineConfig } = require('vite');

/**
 * https://vitejs.dev/config
 */
const config = defineConfig({
    root: Path.join(__dirname, 'src', 'renderer'),
    publicDir: 'public',
    server: {
        port: 8080,
    },
    open: false,
    build: {
        outDir: Path.join(__dirname, 'build', 'renderer'),
        emptyOutDir: true,
    },
	resolve: {
		alias: {
			vue: '@vue/compat',
			'@' : Path.resolve(__dirname, './src/renderer')
		},
	},
	plugins: [vuePlugin()],
});

module.exports = config;
