#!/bin/bash 
pyinstaller --distpath ../main/static/api_dist swatplus_rest_api.py --noconfirm --onefile
pyinstaller --distpath ../main/static/api_dist swatplus_api.py --noconfirm --onefile
cd ../main/static/api_dist
staticx swatplus_api swatplus_api_static
staticx swatplus_rest_api swatplus_rest_api_static
rm swatplus_api
rm swatplus_rest_api
mv swatplus_api_static swatplus_api
mv swatplus_rest_api_static swatplus_rest_api

#If errors, check patchelf version. As of early 2023, version 0.17.2 does NOT work. Instead run version 0.16.1
#pip install patchelf==0.16.1