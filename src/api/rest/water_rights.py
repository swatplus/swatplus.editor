from flask_restful import Resource, reqparse, abort
from playhouse.shortcuts import model_to_dict
from peewee import *

from .base import BaseRestModel
from database.project import base as project_base
from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config
from database.project.water_rights import Water_allocation_wro, Water_allocation_src_ob, Water_allocation_dmd_ob, Water_allocation_dmd_ob_src


class WaterAllocationListApi(BaseRestModel):
	def get(self, project_db):
		table = Water_allocation_wro
		filter_cols = [table.name, table.rule_typ]
		return self.base_paged_list(project_db, table, filter_cols)


class WaterAllocationApi(BaseRestModel):
	def get(self, project_db, id):
		return self.base_get(project_db, id, Water_allocation_wro, 'Water allocation table', back_refs=True, max_depth=3)

	def delete(self, project_db, id):
		return self.base_delete(project_db, id, Water_allocation_wro, 'Water allocation table')

	def put(self, project_db, id):
		return self.base_put(project_db, id, Water_allocation_wro, 'Water allocation table')


class WaterAllocationPostApi(BaseRestModel):
	def post(self, project_db):
		return self.base_post(project_db, Water_allocation_wro, 'Water allocation table')
	

class WaterAllocationSourceApi(BaseRestModel):
	def get(self, project_db, id):
		return self.base_get(project_db, id, Water_allocation_src_ob, 'Water allocation source')

	def delete(self, project_db, id):
		return self.base_delete(project_db, id, Water_allocation_src_ob, 'Water allocation source')

	def put(self, project_db, id):
		return self.base_put(project_db, id, Water_allocation_src_ob, 'Water allocation source')


class WaterAllocationSourcePostApi(BaseRestModel):
	def post(self, project_db):
		return self.base_post(project_db, Water_allocation_src_ob, 'Water allocation source', extra_args=[{'name': 'water_allocation_id', 'type': int}])
	

class WaterAllocationDemandApi(BaseRestModel):
	def get(self, project_db, id):
		return self.base_get(project_db, id, Water_allocation_dmd_ob, 'Water allocation demand object', back_refs=True, max_depth=2)

	def delete(self, project_db, id):
		return self.base_delete(project_db, id, Water_allocation_dmd_ob, 'Water allocation demand object')

	def put(self, project_db, id):
		return self.base_put(project_db, id, Water_allocation_dmd_ob, 'Water allocation demand object')


class WaterAllocationDemandPostApi(BaseRestModel):
	def post(self, project_db):
		return self.base_post(project_db, Water_allocation_dmd_ob, 'Water allocation demand object', extra_args=[{'name': 'water_allocation_id', 'type': int}])
	

class WaterAllocationDemandSourceApi(BaseRestModel):
	def get(self, project_db, id):
		return self.base_get(project_db, id, Water_allocation_dmd_ob_src, 'Water allocation demand source', back_refs=True)

	def delete(self, project_db, id):
		return self.base_delete(project_db, id, Water_allocation_dmd_ob_src, 'Water allocation demand source')

	def put(self, project_db, id):
		return self.base_put(project_db, id, Water_allocation_dmd_ob_src, 'Water allocation demand source')


class WaterAllocationDemandSourcePostApi(BaseRestModel):
	def post(self, project_db):
		return self.base_post(project_db, Water_allocation_dmd_ob_src, 'Water allocation demand source', extra_args=[{'name': 'water_allocation_dmd_ob_id', 'type': int}])