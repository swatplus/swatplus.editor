/**
 * Should match main/preload.ts for typescript support in renderer
 */
export default interface ElectronApi {
	sendMessage: (message: string) => void,
	setWindowTitle: (message: string) => void,
	getGlobals: () => any,
	getAppSettings: () => any,
	addToStore: (key: string, value:any) => void,
	getStoreSetting: (key:string) => any,
	deleteFromStore: (key:string) => void,
	getAppPath: () => string,
	quitApp: () => void,
	pathExists: (directory:string) => boolean,
	joinPaths: (paths:string[]) => string,
	pathDirectoryName: (directory:string) => string,
	openFileOnSystem: (key:string) => void,
	openUrl: (key:string) => void,
	openFileDialog: (options:any) => string[],
	saveFileDialog: (options:any) => string[],
	spawnProcess: (script_name:string, args:string[]) => any,
	processStdout: (callback) => void,
	processStderr: (callback) => void,
	processClose: (callback) => void,
	killProcess: (pid) => void,
	runSwat: (debug:boolean, inputDir:string) => any,
	getSwatPlusToolboxPath: () => string,
	launchSwatPlusToolbox: (projectDb:string) => string
}

declare global {
	interface Window {
		electronApi: ElectronApi,
	}
}
