import Vue, { createApp, configureCompat } from '@vue/compat';
import App from './App.vue'
import router from './router';
import axios from 'axios';

import BootstrapVue from 'bootstrap-vue';

//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome';

//Highcharts
import HighchartsVue from 'highcharts-vue';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import exportingInit from 'highcharts/modules/exporting';
import exportDataInit from 'highcharts/modules/export-data';
import offlineExportInit from 'highcharts/modules/offline-exporting';
import mapInit from 'highcharts/modules/map';

//Local custom components
import { globalMixin } from './plugins/app-mixin';
import ActionBar from './components/ActionBar.vue';
import BackButton from './components/BackButton.vue';
import ErrorAlert from './components/ErrorAlert.vue';
import FileHeader from './components/FileHeader.vue';
import GridView from './components/GridView.vue';
import OpenFile from './components/OpenFile.vue';
import OpenInBrowser from './components/OpenInBrowser.vue';
import PageLoading from './components/PageLoading.vue';
import ProjectContainer from './components/ProjectContainer.vue';
import ReferenceLabel from './components/ReferenceLabel.vue';
import SaveButton from './components/SaveButton.vue';
import SelectFileInput from './components/SelectFileInput.vue';
import SelectFolderInput from './components/SelectFolderInput.vue';
import StackTraceError from './components/StackTraceError.vue';
import TypeAhead from './components/TypeAhead.vue';
import TypeAheadAndGo from './components/TypeAheadAndGo.vue';
import TypeAheadAndBrowse from './components/TypeAheadAndBrowse.vue';
import ConnectGridView from './components/ConnectGridView.vue';

let globals = window.electronApi.getGlobals();
if (globals === undefined) globals = { api_port: 5000 };

configureCompat({
	WATCH_ARRAY: 'suppress-warning',
	RENDER_FUNCTION: 'suppress-warning',
	INSTANCE_LISTENERS: 'suppress-warning',
	COMPONENT_FUNCTIONAL: 'suppress-warning',
	OPTIONS_BEFORE_DESTROY: 'suppress-warning',
	INSTANCE_SCOPED_SLOTS: 'suppress-warning',
	OPTIONS_DATA_MERGE: 'suppress-warning',
	COMPONENT_V_MODEL: 'suppress-warning',
	CUSTOM_DIR: 'suppress-warning',
	INSTANCE_EVENT_EMITTER: 'suppress-warning',
	ATTR_FALSE_VALUE: 'suppress-warning',
	INSTANCE_ATTRS_CLASS_STYLE: 'suppress-warning',
	GLOBAL_PROTOTYPE: 'suppress-warning',
	GLOBAL_EXTEND: 'suppress-warning',
	GLOBAL_MOUNT: 'suppress-warning',
	OPTIONS_DESTROYED: 'suppress-warning',
	INSTANCE_DESTROY: 'suppress-warning',
});

Vue.use(BootstrapVue);
const app = createApp(App);

app.config.globalProperties.$http = axios.create({ baseURL: 'http://localhost:' + globals.api_port + '/' });
app.config.globalProperties.append = (path, pathToAppend) => path + (path.endsWith('/') ? '' : '/') + pathToAppend;
app.mixin(globalMixin);
app.use(router);

//Font Awesome
library.add(fas, far, fab);
app.component('font-awesome-icon', FontAwesomeIcon);
app.component('font-awesome-layers', FontAwesomeLayers);
app.component('font-awesome-layers-text', FontAwesomeLayersText);

//Highcharts
app.use(HighchartsVue);
Highcharts.setOptions({
	chart: {
		style: {
			fontFamily: '"Segoe UI", "Helvetica Neue", Arial, sans-serif'
		}
	},
	lang: {
		thousandsSep: '\u002c'
	},
	title: {
        style: {
			fontSize: '16px',
			fontWeight: 'bold'
        }
    }
});
HighchartsMore(Highcharts);
exportingInit(Highcharts);
exportDataInit(Highcharts);
offlineExportInit(Highcharts);
mapInit(Highcharts);

//Local custom components
app.component('action-bar', ActionBar);
app.component('back-button', BackButton);
app.component('error-alert', ErrorAlert);
app.component('file-header', FileHeader);
app.component('grid-view', GridView);
app.component('open-file', OpenFile);
app.component('open-in-browser', OpenInBrowser);
app.component('page-loading', PageLoading);
app.component('project-container', ProjectContainer);
app.component('reference-label', ReferenceLabel);
app.component('save-button', SaveButton);
app.component('select-file-input', SelectFileInput);
app.component('select-folder-input', SelectFolderInput);
app.component('stack-trace-error', StackTraceError);
app.component('type-ahead', TypeAhead);
app.component('type-ahead-and-go', TypeAheadAndGo);
app.component('type-ahead-and-browse', TypeAheadAndBrowse);
app.component('connect-grid-view', ConnectGridView);

app.mount('#app');
