import { store } from './store.js';
import moment from 'moment';
const electron = window.electronApi;

export const globalMixin = {
	data() {
		return {
			store,
            version: null,
            swatplus: null,
            python: false,
            pythonPath: null,
            globals: {
				dev_mode: false,
				platform: null,
				project_db: null,
				api_port: null
			},
			objTypeRouteTable: {
				'hru': { path: '/edit/hrus/edit/', name: 'HrusEdit' },
				'hlt': { path: '/edit/hrus-lte/edit/', name: 'HrusLteEdit' },
				'ru': { path: '/edit/routing_unit/edit/', name: 'RoutingUnitEdit' },
				'aqu': { path: '/edit/aquifers/edit/', name: 'AquifersEdit' },
				'cha': { path: '/edit/channels/edit/', name: 'ChannelsEdit' },
				'res': { path: '/edit/reservoirs/edit/', name: 'ReservoirsEdit' },
				'rec': { path: '/edit/recall/edit/', name: 'RecallEdit' },
				'exc': { path: '/edit/exco/edit/', name: 'ExcoEdit' },
				'dr': { path: '/edit/dr/edit/', name: 'DelratioEdit' },
				'out': { path: '#', name: '#' },
                'mfl': { path: '#', name: '#' },
                'sdc': { path: '/edit/channels/edit/', name: 'ChannelsEdit' }
			},
			noObjTypeRoutes: [
				'hlt', 'rec', 'out', 'mfl'
			],
			objTypeToConTable: {
				'hru': 'hru_con',
				'hlt': 'hru_lte_con',
				'ru': 'rtu_con',
				'aqu': 'aqu_con',
				'cha': 'cha_con',
				'res': 'res_con',
				'rec': 'rec_con',
				'exc': 'exco_con',
				'dr': 'dr_con',
				'out': 'out_con',
                'mfl': 'modflow_con',
                'sdc': 'chandeg_con'
			},
            commonMessages: {
                leaveWarning: 'Click to view details. Warning: any unsaved changes on this page will be lost!'
            }
		}
	},
	created() {
		this.globals = electron.getGlobals();
		let appsettings = electron.getAppSettings();

        this.version = appsettings.version;
        this.swatplus = appsettings.swatplus;
        this.python = appsettings.python;
        this.pythonPath = appsettings.pythonPath;         
	},
	computed: {
		projectDbUrl() {
			if (this.store.currentProject === null) return null;
			return encodeURIComponent(this.store.currentProject.projectDb);
        },
        datasetsDbUrl() {
            if (this.store.currentProject === null) return null;
			return encodeURIComponent(this.store.currentProject.datasetsDb);
        },
        hasCurrentProject() {
            return !(this.store.currentProject === undefined || this.store.currentProject.projectDb === null);
        },
        currentProjectSupported() {
            if (!this.hasCurrentProject) return false;
            let versionSupport = this.getVersionSupport(this.store.currentProject.version);
            return versionSupport.supported;
        },
		getValidState() {
			return (prop) => {
				return prop.$dirty ? !prop.$error : null;
			}
		},
		requiredFeedback() {
			return (prop) => {
				if (!prop.required) return 'Required';
			}
        },
        txtInOut() {
            if (!this.hasCurrentProject) return null;
            let d = electron.pathDirectoryName(this.store.currentProject.projectDb);
            let txtinout = electron.joinPaths([d, 'Scenarios', 'Default', 'TxtInOut']);

            if (!this.pathExists(txtinout))
                txtinout = d;

            return txtinout;
        },
        appPath() {
            return electron.getAppPath().replace('app.asar', 'app.asar.unpacked');
        },
        projectPath() {
			if (!this.hasCurrentProject) return null;
            return electron.pathDirectoryName(this.store.currentProject.projectDb);
        },
        wgnDbPath() {
            let appPath = this.appPath;
            let installPath = electron.joinPaths([appPath, '../../../Databases/swatplus_wgn.sqlite']);
            let installPathMac = electron.joinPaths([appPath, '../../../../../Databases/swatplus_wgn.sqlite']);
            this.log(`WGN Install Path: ${installPath}`);
            let searchPaths = [
                installPath,
                installPathMac,
                'C:/SWAT/SWATPlus/Databases/swatplus_wgn.sqlite'
            ];

            for (let p of searchPaths) {
                if (this.pathExists(p)) return p;
            }

            return null;
        },
		currentProject() {
			return this.store.currentProject;
		}
	},
	methods: {
		setWindowTitle() {
            let title = `SWAT+ Editor ${this.version}`;
            if (this.store.currentProject !== undefined && !this.isNullOrEmpty(this.store.currentProject.name))  title += ' / ' + this.store.currentProject.name;// + ' (' + this.store.currentProject.projectDb + ')';
            electron.setWindowTitle(title);
        },
        isNullOrEmpty(value, checkNullStr=false) {
            return value === '' || value === null || value === undefined || (checkNullStr && value === 'null');
        },
        valueIfNull(v, d) {
            return this.isNullOrEmpty(v) ? d : v;
        },
        pathExists(path) {
            return electron.pathExists(path);
        },
        joinPaths(path1, path2) {
            return electron.joinPaths([path1, path2]);
        },
		getAutoComplete(type, name) {
			if (this.isNullOrEmpty(name))
				return this.$http.get(`autocomplete-np/${type}/${this.projectDbUrl}`);
			return this.$http.get(`autocomplete/${type}/${name}/${this.projectDbUrl}`);
		},
		getAutoCompleteId(type, name) {
			return this.$http.get(`autocomplete/id/${type}/${name}/${this.projectDbUrl}`);
		},
		getDataResponse(response) {
			return response.data;
		},
        getVersionSupport(version) {
            let support = {
                supported: false,
                updatable: false,
                error: null
            };

            if (this.version === null) {
                support.error = 'Software version is not set.';
            }
            else if (this.version === version) {
                support.supported = true;
            }
            else {
                let softwareMajor = Number(this.version.substring(0, 3));
                let myMajor = Number(version.substring(0, 3));

                if (softwareMajor > myMajor) {
                    support.updatable = true;
                    support.error = `Your project was made using an earlier version of SWAT+ Editor. Your project is version ${version} and the editor is version ${this.version}.`;
                } else if (softwareMajor < myMajor) {
                    support.supported = true;
                    support.error = `Your project was made using a version of SWAT+ Editor that is greater than the current version. Your project is version ${version} and the editor is version ${this.version}. You may encounter errors if the model has changed. Proceed at your own risk.`;
                } else {
                    support.supported = true;
                }
            }
            this.log(support);
            return support;
        },
        validName(name) {
            if (this.isNullOrEmpty(name)) return name;
            return name.replace(/ /g,"_");
        },
        validFileName(name) {
            return name.replace(/([^a-z0-9]+)/gi, '');
        },
        capitalizeFirstLetter(s) {
            return s.charAt(0).toUpperCase() + s.slice(1);
        },
        logError(error, defaultMessage='') {
            console.log(error);
            var message = '';

            if (error.response) {
                console.log(error.response);
                var r = error.response;
                message = r.data != null && r.data.message != null ? r.data.message : '';
            } else if (error.data) {
                console.log(error.data);
                message = error.data.message != null ? error.data.message : '';
            } else if (error.request) {
                console.log(error.request);
            }

            if (message == '' && defaultMessage == '')
                return null;

            return defaultMessage + ' ' + message;
        },
		numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return parts.join(".");
		},
		numberFixed(num, decimals) {
			if (this.isNullOrEmpty(num)) return num;
			return num.toFixed(decimals);
		},
		numberFormat(value, decimals = 1, units = '', ifNull = '') {
			if (isNaN(Number(value))) return value;
            if (this.isNullOrEmpty(value)) return ifNull;
			return this.numberWithCommas(Number(value).toFixed(decimals)) + units;
		},
		roundNumber(num) {
			return Math.round((num + Number.EPSILON) * 100) / 100;
        },
        toDecimals(val, precision) {
            return Number(val.toFixed(precision));
        },
		isDevMode() {
			return this.globals.dev_mode;
		},
		log(message) {
			if (this.isDevMode()) {
				console.log(message);
			}
		},
        addSetting(key, value) {
            electron.addToStore(key, JSON.stringify(value));
        },
        getSetting(key) {
			try {
				return JSON.parse(electron.getStoreSetting(key));
			} catch (error) {
				this.deleteSetting(key);
				return undefined;
			}
        },
        deleteSetting(key) {
            electron.deleteFromStore(key);
        },
        getMostRecentProject() {
            let recent = this.getSetting('recentProjects');
            if (recent === undefined || recent.length < 1) return undefined;
            return recent[0];
        },
        pushRecentProject(project) {
            let recent = this.getSetting('recentProjects');
			this.log('inside pushRecentProject');
            this.log(recent);
            this.log(project);
            if (recent === undefined) recent = [];

            let exists = recent.filter(function(el) { return el.projectDb === project.projectDb; })[0];
            this.log(exists);
            if (exists) {
                let newProjects = recent.filter(function(el) { return el.projectDb !== project.projectDb; });
                newProjects.unshift(project);

                if (newProjects.length > 4)
                    newProjects.pop();

                this.addSetting('recentProjects', newProjects);
            }

            if (exists === undefined) {
                recent.unshift(project);

                if (recent.length > 4)
                    recent.pop();

				this.log(recent);
                this.addSetting('recentProjects', recent);
            }
        },
        deleteRecentProject(project) {
            let recent = this.getSetting('recentProjects');
            if (recent === undefined)
                recent = [];

            let filtered = recent.filter(function(el) { return el.projectDb !== project.projectDb; });
            this.addSetting('recentProjects', filtered);
            this.log('removed');
            this.log(filtered);
            return filtered;
        },
        getRecentProjects() {
            let recent = this.getSetting('recentProjects');
			this.log('getRecentProject');
			this.log(recent);
            if (recent === undefined) recent = [];
            return recent;
        },
        getPieChart(title, data, seriesLabel = 'Area') {
            return {
                plotOptions: { pie: { dataLabels: { enabled: false }, showInLegend: true } },
                title: { text: title },
                tooltip: { pointFormat: '{series.name}: <b>{point.percentage:,.1f}% ({point.y:,.1f} ha)</b>' },
                series: [{ data: data, name: seriesLabel, type: 'pie' }]
            };
        },
		joinPath: function(filePath, fileName) {
			if (this.isNullOrEmpty(filePath))
				return fileName;
				
			return electron.joinPaths([filePath, fileName]);
		},
		getObjTypeRoute(item) {
			let route = this.objTypeRouteTable[item.obj_typ].path;
			if (route === '#')
				return route;

			if ('obj_id' in item)
				return route + item.obj_id;
			else if ('obj_typ_no' in item)
				return route + item.obj_typ_no;

			return '#';
        },
        setToNameProp(item) {
            if (this.isNullOrEmpty(item)) return '';
            return item['name'];
        },
        setVars(item, vars) {
            let keys = Object.keys(vars);
            for (let k of keys) {
                let v = vars[k];
                item[k] = v.type == 'string' ? v.default_text : v.default_value;
            }
            return item;
        },
		async exit() {
			//await this.$http.get('shutdown');
			electron.quitApp();
        },
		dateFormat(value, format = 'llll') {
			if (value) {
				return moment(String(value)).format(format);
			}
		},
		getMeta(field, item) {
			let label = field[0].toUpperCase() + field.substring(1);
			let css = 'text-right';
			let formatter = (value) => { return Number.isInteger(Number(value)) ? value : Number(value).toFixed(3) }

			let textFields = [
				'name',
				'description'
			];

			if (textFields.includes(field) || isNaN(item)) {
				css = 'text-left';
				formatter = undefined;
			}

			return {
				label: label,
				css: css,
				formatter: formatter
			};
		}
	}
}