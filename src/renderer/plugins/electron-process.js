const electron = window.electronApi;

export const runProcess = {
    methods: {
        runApiProc(script_name, args) {
			this.log("starting process");
			return electron.spawnProcess(script_name, args);
		},
		runSwatProc(inputDir, debug) {
			return electron.runSwat(debug, inputDir);
		},
		processStdout: (callback) => electron.processStdout(callback),
		processStderr: (callback) => electron.processStderr(callback),
		processClose: (callback) => electron.processClose(callback),
		killProcess(pid) {
			electron.killProcess(pid);
		},
		getApiOutput(data) {
			try {
				return JSON.parse(data);
			} catch (error) {
				return data;
			}
		},
        resultsPath(inputDir) {
			if (this.isNullOrEmpty(inputDir)) return '';
            let d = inputDir.replace(/\\/g,"/");
			let resultsPath = electron.joinPaths([d, '../', 'Results']);

			if (!this.pathExists(resultsPath))
                resultsPath = inputDir;

            return resultsPath;
        },
        outputDbPath(inputDir) {
            return electron.joinPaths([this.resultsPath(inputDir), 'swatplus_output.sqlite']);
        }
    }
};