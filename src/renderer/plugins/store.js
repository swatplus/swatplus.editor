import { reactive } from 'vue';

export const store = reactive({
	currentProject: {
		projectDb: null,
		datasetsDb: null,
		name: null,
		description: null,
		version: null,
		isLte: false
	},
	setCurrentProject(project) {
		this.currentProject = project;
	}
})