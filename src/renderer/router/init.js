import InitPlant from '../views/edit/init/Plant.vue';
import InitPlantEdit from '../views/edit/init/PlantEdit.vue';
import InitPlantCreate from '../views/edit/init/PlantCreate.vue';

import InitSoilPlant from '../views/edit/init/SoilPlant.vue';
import InitSoilPlantEdit from '../views/edit/init/SoilPlantEdit.vue';
import InitSoilPlantCreate from '../views/edit/init/SoilPlantCreate.vue';

import InitOMWater from '../views/edit/init/OMWater.vue';
import InitOMWaterEdit from '../views/edit/init/OMWaterEdit.vue';
import InitOMWaterCreate from '../views/edit/init/OMWaterCreate.vue';

import InitConstituents from '../views/edit/init/constituents/Constituents.vue';
import InitConstituentsPestHruIni from '../views/edit/init/constituents/PestHruIni.vue';
import InitConstituentsPestWaterIni from '../views/edit/init/constituents/PestWaterIni.vue';
import InitConstituentsPathHruIni from '../views/edit/init/constituents/PathHruIni.vue';
import InitConstituentsPathWaterIni from '../views/edit/init/constituents/PathWaterIni.vue';

export default [
	{ 
		path: 'init/plant', name: 'InitPlant', component: InitPlant, 
		children: [
			{ path: 'edit/:id', name: 'InitPlantEdit', component: InitPlantEdit },
			{ path: 'create', name: 'InitPlantCreate', component: InitPlantCreate }
		] 
	},
	{ 
		path: 'init/soil_plant', name: 'InitSoilPlant', component: InitSoilPlant, 
		children: [
			{ path: 'edit/:id', name: 'InitSoilPlantEdit', component: InitSoilPlantEdit },
			{ path: 'create', name: 'InitSoilPlantCreate', component: InitSoilPlantCreate }
		]
	},
	{ 
		path: 'init/om_water', name: 'InitOMWater', component: InitOMWater, 
		children: [
			{ path: 'edit/:id', name: 'InitOMWaterEdit', component: InitOMWaterEdit },
			{ path: 'create', name: 'InitOMWaterCreate', component: InitOMWaterCreate }
		] 
	},
	{ 
		path: 'init/constituents', name: 'InitConstituents', component: InitConstituents, 
		children: [
			{ path: 'pest-hru', name: 'InitConstituentsPestHruIni', component: InitConstituentsPestHruIni },
			{ path: 'pest-water', name: 'InitConstituentsPestWaterIni', component: InitConstituentsPestWaterIni },
			{ path: 'path-hru', name: 'InitConstituentsPathHruIni', component: InitConstituentsPathHruIni },
			{ path: 'path-water', name: 'InitConstituentsPathWaterIni', component: InitConstituentsPathWaterIni }
		] 
	},
];