import CalParms from '../views/edit/change/hard/CalParms.vue';
import CalParmsEdit from '../views/edit/change/hard/CalParmsEdit.vue';

import Calibration from '../views/edit/change/hard/Calibration.vue';
import CalibrationEdit from '../views/edit/change/hard/CalibrationEdit.vue';
import CalibrationCreate from '../views/edit/change/hard/CalibrationCreate.vue';

import CalCodes from '../views/edit/change/soft/Codes.vue';

import WbParms from '../views/edit/change/soft/WbParms.vue';
import WbParmsEdit from '../views/edit/change/soft/WbParmsEdit.vue';
import WbParmsCreate from '../views/edit/change/soft/WbParmsCreate.vue';

import ChsedParms from '../views/edit/change/soft/ChsedParms.vue';
import ChsedParmsEdit from '../views/edit/change/soft/ChsedParmsEdit.vue';
import ChsedParmsCreate from '../views/edit/change/soft/ChsedParmsCreate.vue';

import PlantParms from '../views/edit/change/soft/PlantParms.vue';
import PlantParmsEdit from '../views/edit/change/soft/PlantParmsEdit.vue';
import PlantParmsCreate from '../views/edit/change/soft/PlantParmsCreate.vue';

import WaterBalance from '../views/edit/change/soft/WaterBalance.vue';
import WaterBalanceEdit from '../views/edit/change/soft/WaterBalanceEdit.vue';
import WaterBalanceCreate from '../views/edit/change/soft/WaterBalanceCreate.vue';

import ChsedBudget from '../views/edit/change/soft/ChsedBudget.vue';
import ChsedBudgetEdit from '../views/edit/change/soft/ChsedBudgetEdit.vue';
import ChsedBudgetCreate from '../views/edit/change/soft/ChsedBudgetCreate.vue';

import PlantGro from '../views/edit/change/soft/PlantGro.vue';
import PlantGroEdit from '../views/edit/change/soft/PlantGroEdit.vue';
import PlantGroCreate from '../views/edit/change/soft/PlantGroCreate.vue';

export default [
	{ 
		path: 'change/hard', name: 'HardCalibration', component: Calibration,
		children: [
			{ path: 'edit/:id', name: 'HardCalibrationEdit', component: CalibrationEdit },
			{ path: 'create', name: 'HardCalibrationCreate', component: CalibrationCreate },
			{ 
				path: 'parms', name: 'HardCalibrationParms', component: CalParms,
				children: [
					{ path: 'edit/:id', name: 'HardCalibrationParmsEdit', component: CalParmsEdit }
				]  
			},
		]  
	},
	{ 
		path: 'change/soft', name: 'SoftCalibration', component: CalCodes,
		children: [
			{ 
				path: 'wb', name: 'SoftCalibrationWaterBalance', component: WaterBalance,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationWaterBalanceEdit', component: WaterBalanceEdit },
					{ path: 'create', name: 'SoftCalibrationWaterBalanceCreate', component: WaterBalanceCreate }
				]  
			},
			{ 
				path: 'wbparms', name: 'SoftCalibrationWbParms', component: WbParms,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationWbParmsEdit', component: WbParmsEdit },
					{ path: 'create', name: 'SoftCalibrationWbParmsCreate', component: WbParmsCreate }
				]  
			},
			{ 
				path: 'chsed', name: 'SoftCalibrationChsedBudget', component: ChsedBudget,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationChsedBudgetEdit', component: ChsedBudgetEdit },
					{ path: 'create', name: 'SoftCalibrationChsedBudgetCreate', component: ChsedBudgetCreate }
				]  
			},
			{ 
				path: 'chsedparms', name: 'SoftCalibrationChsedParms', component: ChsedParms,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationChsedParmsEdit', component: ChsedParmsEdit },
					{ path: 'create', name: 'SoftCalibrationChsedParmsCreate', component: ChsedParmsCreate }
				]  
			},
			{ 
				path: 'plant', name: 'SoftCalibrationPlantGro', component: PlantGro,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationPlantGroEdit', component: PlantGroEdit },
					{ path: 'create', name: 'SoftCalibrationPlantGroCreate', component: PlantGroCreate }
				]  
			},
			{ 
				path: 'plantparms', name: 'SoftCalibrationPlantParms', component: PlantParms,
				children: [
					{ path: 'edit/:id', name: 'SoftCalibrationPlantParmsEdit', component: PlantParmsEdit },
					{ path: 'create', name: 'SoftCalibrationPlantParmsCreate', component: PlantParmsCreate }
				]  
			},
		]
	}
];