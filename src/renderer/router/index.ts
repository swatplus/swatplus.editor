import { createRouter, createWebHashHistory } from 'vue-router';
import Setup from '../views/Setup.vue';
import Help from '../views/Help.vue';
import Run from '../views/Run.vue';
import Check from '../views/Check.vue';
import Edit from '../views/edit/Edit.vue';
import TableBrowser from '../views/TableBrowser.vue';

import basin from './basin';
import change from './change';
import climate from './climate';
import connect from './connect';
import decision_table from './decision_table';
import db from './db';
import hydrology from './hydrology';
import init from './init';
import lum from './lum';
import regions from './regions';
import soils from './soils';
import structural from './structural';
import water_rights from './water_rights';

const editRoutes = climate.concat(basin, change, connect, decision_table, db, hydrology, init, lum, regions, soils, structural, water_rights);

export default createRouter({
	history: createWebHashHistory(),
	linkActiveClass: 'active',
	routes: [
		{ 
			path: '/', name: 'Setup', component: Setup,
			children: [
				{ path: 'help', name: 'Help', component: Help },
				{ path: 'run', name: 'Run', component: Run },
				{ path: 'check', name: 'Check', component: Check },
				{ 
					path: 'edit', name: 'Edit', component: Edit,
					children: editRoutes
				},
			]
		},
		{ path: '/table-browser', name: 'TableBrowser', component: TableBrowser }
	],
})